import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    username: "Seha",
    password: "12345",
    isLogin: "false",
  },
  getters: {
    getIsLogin: (state) => {
      return state.isLogin;
    },
  },
  actions: {
    updateIsLogin: ({ commit }, [username, password]) => {
      commit("setIsLogin", [username, password]);
    },
    logoutStore: ({ commit }) => {
      commit("logoutStore");
    },
  },
  mutations: {
    setIsLogin: (state, [username, password]) => {
      if (state.username == username && state.password == password) {
        state.isLogin = true;
      } else {
        state.isLogin = false;
      }
    },
    logoutStore: (state) => {
      state.isLogin = false;
    },
  },
});

export default store;
