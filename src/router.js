import Vue from "vue";
import VueRouter from "vue-router";

import Home from "./views/Home";
import Login from "./views/Login";
import Dashboard from "./views/Dashboard";
import Register from "./views/Register";

Vue.use(VueRouter);

const routes = [
  { name: "Home", path: "/", component: Home },
  { name: "Login", path: "/Login", component: Login },
  { name: "Register", path: "/Register", component: Register },
  { name: "Dashboard", path: "/Dashboard", component: Dashboard },
];

export const router = new VueRouter({
  mode: "history",
  routes,
});
